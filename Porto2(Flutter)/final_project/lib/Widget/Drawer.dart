import 'package:final_project/Auth/LoginScreen.dart';
import 'package:final_project/View/Cart.dart';
import 'package:final_project/View/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DrawerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

    logout() {
      _firebaseAuth.signOut().then((value) => Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginScreen())));
    }

    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text("Irvan Hauwerich"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("assets/images/hau.jpg"),
            ),
            accountEmail: Text("irvanhau@gmail.com"),
          ),
          DrawerListTile(
            iconData: Icons.supervisor_account_sharp,
            onTilePressed: () {
              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => HomePage()));
            },
            title: "Dashboard",
          ),
          DrawerListTile(
            iconData: Icons.shopping_cart,
            onTilePressed: () {
              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Cart()));
            },
            title: "Cart",
          ),
          DrawerListTile(
            iconData: Icons.logout,
            onTilePressed: () {
              logout();
            },
            title: "Logout",
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
