import 'dart:async';

import 'package:final_project/Auth/LoginScreen.dart';
import 'package:flutter/material.dart';

import '../View/HomePage.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    openSplashScreen();
  }

  openSplashScreen() async {
    //bisa diganti beberapa detik sesuai keinginan
    var durasiSplash = const Duration(seconds: 2);

    return Timer(durasiSplash, () {
      //pindah ke halaman home
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return LoginScreen();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(118, 186, 153, 1),
      body: Center(
          child: Text(
        "Ecommerce",
        style: TextStyle(
          fontSize: 60,
          fontWeight: FontWeight.bold,
        ),
      )),
    );
  }
}
