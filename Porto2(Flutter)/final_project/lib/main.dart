// import 'package:final_project/View/DetailScreen.dart';
import 'package:final_project/View/HomePage.dart';
import 'package:final_project/Auth/LoginScreen.dart';
import 'package:final_project/Auth/Register.dart';
import 'package:final_project/Widget/SplashScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Apps',
      theme: ThemeData(
        fontFamily: 'Poppins',
      ),
      home: SplashScreenPage(),
    );
  }
}
