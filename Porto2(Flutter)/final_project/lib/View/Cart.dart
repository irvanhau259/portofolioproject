import 'package:final_project/Widget/Drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import '../Model/Cart.dart';

class Cart extends StatelessWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shopping Cart"),
      ),
      drawer: DrawerScreen(),
      body: ListView.separated(
        itemBuilder: (ctx, i) {
          return ListTile(
            leading: Container(
              // width: 150,
              // height: 150,
              child: Image.network(
                items[i].imageUrl!,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(items[i].name!),
            trailing: Text(items[i].harga!),
          );
        },
        separatorBuilder: (ctx, i) {
          return Divider(
            color: Colors.black,
            height: 4,
          );
        },
        itemCount: items.length,
      ),
    );
  }
}
