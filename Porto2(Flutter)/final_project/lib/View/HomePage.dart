import 'package:final_project/Model/Product.dart';
import 'package:final_project/Widget/Drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("HomeScreen"),
        ),
        drawer: DrawerScreen(),
        body: GridView.count(
          crossAxisCount: 2,
          children: [
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
            ProductCard(
              imageUrl:
                  "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
              name: "Keyboard Gaming",
              deskripsi: "Keyboard Gaming laris di seluruh negara",
              harga: "Rp. 200000",
            ),
          ],
        ));
  }
}
