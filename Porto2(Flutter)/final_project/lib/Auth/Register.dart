import 'package:final_project/Auth/LoginScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../View/HomePage.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool? _passwordVisible;

  Color nature = Color.fromRGBO(118, 186, 153, 1);
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  registSubmit() async {
    try {
      _firebaseAuth
          .createUserWithEmailAndPassword(
              email: _emailController.text, password: _passwordController.text)
          .then((value) => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => LoginScreen())));
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    _passwordVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            Column(
              children: [
                SizedBox(height: 55),
                Center(
                  child: Text(
                    "REGISTER",
                    style: TextStyle(
                      fontSize: 36,
                      color: nature,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(height: 21),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Email",
                    ),
                  ),
                ),
                SizedBox(height: 25),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: _passwordController,
                    obscureText: !_passwordVisible!,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible!;
                          });
                        },
                        child: Icon(_passwordVisible!
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: new BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: nature,
                      spreadRadius: 0,
                      blurRadius: 10,
                      offset: Offset(3, 5),
                    )
                  ]),
                  child: ElevatedButton(
                    child: Text(
                      "Register",
                      style: TextStyle(
                        color: nature,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    style: raisedButtonStyle,
                    onPressed: () {
                      registSubmit();
                    },
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: new BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: nature,
                      spreadRadius: 0,
                      blurRadius: 10,
                      offset: Offset(3, 5),
                    )
                  ]),
                  child: ElevatedButton(
                    child: Text(
                      "Back to Login",
                      style: TextStyle(
                        color: nature,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    style: raisedButtonStyle,
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()));
                    },
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Color.fromRGBO(173, 207, 159, 1),
    primary: Color.fromRGBO(206, 216, 158, 1),
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(6))));
