import 'package:final_project/Auth/LoginScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final TextEditingController _emailController = TextEditingController();
    Color nature = Color.fromRGBO(118, 186, 153, 1);
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    resetPassword() async {
      try {
        _firebaseAuth.sendPasswordResetEmail(email: _emailController.text).then(
            (value) => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => LoginScreen())));
      } catch (e) {
        print(e);
      }
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            Column(
              // mainAxisSize: MainAxisSize.min,
              // mainAxisAlignment: MainAxisAlignment.end,
              // crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(height: 105),
                Center(
                  child: Text(
                    "RESET PASSWORD",
                    style: TextStyle(
                      fontSize: 36,
                      color: nature,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(height: 21),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Email",
                    ),
                  ),
                ),
                SizedBox(height: 15),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: new BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: nature,
                      spreadRadius: 0,
                      blurRadius: 10,
                      offset: Offset(3, 5),
                    )
                  ]),
                  child: ElevatedButton(
                    child: Text(
                      "Reset Password",
                      style: TextStyle(
                        color: nature,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    style: raisedButtonStyle,
                    onPressed: () {
                      resetPassword();
                    },
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: new BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: nature,
                      spreadRadius: 0,
                      blurRadius: 10,
                      offset: Offset(3, 5),
                    )
                  ]),
                  child: ElevatedButton(
                    child: Text(
                      "Back to Login",
                      style: TextStyle(
                        color: nature,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    style: raisedButtonStyle,
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()));
                    },
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Color.fromRGBO(173, 207, 159, 1),
    primary: Color.fromRGBO(206, 216, 158, 1),
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(6))));
