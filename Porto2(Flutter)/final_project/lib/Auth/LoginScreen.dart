import 'package:final_project/View/HomePage.dart';
import 'package:final_project/Auth/Register.dart';
import 'package:final_project/Auth/ResetPassword.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'Register.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool? _passwordVisible;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  loginSubmit() async {
    try {
      _firebaseAuth
          .signInWithEmailAndPassword(
              email: _emailController.text, password: _passwordController.text)
          .then((value) => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage())));
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    _passwordVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color nature = Color.fromRGBO(118, 186, 153, 1);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Flex(
          direction: Axis.vertical,
          children: [
            Column(
              children: [
                SizedBox(height: 55),
                Center(
                  child: Text(
                    "LOGIN",
                    style: TextStyle(
                      fontSize: 36,
                      color: nature,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(height: 21),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Email",
                    ),
                  ),
                ),
                SizedBox(height: 25),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: _passwordController,
                    obscureText: !_passwordVisible!,
                    onEditingComplete: loginSubmit,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible!;
                          });
                        },
                        child: Icon(_passwordVisible!
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  child: TextButton(
                    child: Text(
                      "Don't have an account yet? Sign Up",
                      style: TextStyle(
                        fontSize: 16,
                        color: nature,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => Register()));
                    },
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: new BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: nature,
                      spreadRadius: 0,
                      blurRadius: 20,
                      offset: Offset(5, 5),
                    )
                  ]),
                  child: ElevatedButton(
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: nature,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    style: raisedButtonStyle,
                    onPressed: () {
                      loginSubmit();
                    },
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  child: TextButton(
                    child: Text(
                      "Reset Password",
                      style: TextStyle(
                        fontSize: 16,
                        color: nature,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (context) => ResetPassword()));
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Color.fromRGBO(173, 207, 159, 1),
    primary: Color.fromRGBO(206, 216, 158, 1),
    minimumSize: Size(88, 36),
    padding: EdgeInsets.symmetric(horizontal: 16),
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(6))));
