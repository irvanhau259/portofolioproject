// import 'package:final_project/View/DetailScreen.dart';
import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget {
  String? name;
  String? deskripsi;
  String? imageUrl;
  String? harga;

  ProductCard({
    this.name,
    this.deskripsi,
    this.imageUrl,
    this.harga,
  });

  @override
  Widget build(BuildContext context) {
    return Flex(
      direction: Axis.vertical,
      children: [
        Card(
          //menambahkan bayangan
          elevation: 10,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                          imageUrl!,
                        ),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Center(child: Text(name!)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  deskripsi!,
                  textAlign: TextAlign.center,
                ),
              ),
              // Spacer(),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Center(child: Text(harga!)),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
