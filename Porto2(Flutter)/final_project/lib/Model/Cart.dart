class CartModel {
  final String? name;
  final String? harga;
  final String? imageUrl;

  CartModel({this.name, this.harga, this.imageUrl});
}

final List<CartModel> items = [
  CartModel(
    name: "Keyboard Gaming",
    harga: "Rp. 200.000",
    imageUrl:
        "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
  ),
  CartModel(
    name: "Keyboard Gaming",
    harga: "Rp. 200.000",
    imageUrl:
        "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
  ),
  CartModel(
    name: "Keyboard Gaming",
    harga: "Rp. 200.000",
    imageUrl:
        "https://www.tradeinn.com/f/13831/138316799/mars-gaming-mkminibes-rgb-gaming-mechanical-keyboard.jpg",
  ),
];
