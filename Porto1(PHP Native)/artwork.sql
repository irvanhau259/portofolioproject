-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 19 Agu 2022 pada 01.56
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `artwork`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artwork_draft`
--

CREATE TABLE `artwork_draft` (
  `job_id` int(11) NOT NULL,
  `drafter_id` int(11) NOT NULL,
  `artwork_draft` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `artwork_draft`
--

INSERT INTO `artwork_draft` (`job_id`, `drafter_id`, `artwork_draft`) VALUES
(1, 1, 'macbook2.jfif'),
(2, 1, 'macbook2.jfif'),
(2, 1, 'macbook2.jfif'),
(3, 1, 'macbook2.jfif'),
(3, 1, 'macbook2.jfif'),
(2, 1, 'macbook2.jfif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `artwork_final`
--

CREATE TABLE `artwork_final` (
  `job_id` int(11) NOT NULL,
  `corrector_id` int(11) NOT NULL,
  `artwork_final` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `artwork_final`
--

INSERT INTO `artwork_final` (`job_id`, `corrector_id`, `artwork_final`) VALUES
(2, 1, 'voli2 (1) (3).jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `attachment`
--

CREATE TABLE `attachment` (
  `job_id` int(11) NOT NULL,
  `specialist_id` int(11) NOT NULL,
  `attachment` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `attachment`
--

INSERT INTO `attachment` (`job_id`, `specialist_id`, `attachment`) VALUES
(1, 1, 'tenismeja2.jpg'),
(2, 1, 'voli2 (1).jpg'),
(3, 1, 'voli2 (1).jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `corrector`
--

CREATE TABLE `corrector` (
  `id` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(25) NOT NULL,
  `join_date` date NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `corrector`
--

INSERT INTO `corrector` (`id`, `nik`, `full_name`, `username`, `join_date`, `password`) VALUES
(1, 'CR00012345', 'Corector', 'corector', '2022-08-09', 'password'),
(2, 'CR21354', 'Wendy', 'gardiel', '2022-10-22', 'gardiel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `corrector_comment`
--

CREATE TABLE `corrector_comment` (
  `corrector_id` int(11) NOT NULL,
  `jo_number` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `corrector_comment`
--

INSERT INTO `corrector_comment` (`corrector_id`, `jo_number`, `comment`) VALUES
(1, 1, 'TOLONG DIPERBAIKI'),
(1, 1, 'TOLONG DIPERBAIKI'),
(1, 2, 'TOLONG DIPERBAIKI'),
(1, 3, 'TOLONG DIREVISI'),
(1, 3, 'TOLONG DIREVISI LAGI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `drafter`
--

CREATE TABLE `drafter` (
  `id` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(25) NOT NULL,
  `join_date` date NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `drafter`
--

INSERT INTO `drafter` (`id`, `nik`, `full_name`, `username`, `join_date`, `password`) VALUES
(1, 'DR00012345', 'drafter', 'drafter', '2022-08-09', 'drafter'),
(2, 'DR12345', 'Dimas', 'azizir', '2022-10-22', 'azizir');

-- --------------------------------------------------------

--
-- Struktur dari tabel `drug`
--

CREATE TABLE `drug` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `drug`
--

INSERT INTO `drug` (`id`, `name`) VALUES
(1, 'Obat Bebas'),
(2, 'Obat Kerang'),
(3, 'Obat Bebas Terbatas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_order`
--

CREATE TABLE `job_order` (
  `jo_number` int(11) NOT NULL,
  `artwork_status` varchar(50) NOT NULL,
  `input_date` date NOT NULL,
  `due_date` date NOT NULL,
  `prod_name` varchar(50) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `drug_id` int(10) NOT NULL,
  `dosage_form` varchar(50) NOT NULL,
  `roa` varchar(50) NOT NULL,
  `storage` varchar(50) NOT NULL,
  `manufactured` varchar(50) NOT NULL,
  `import` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `marketed` varchar(50) NOT NULL,
  `cc_number` varchar(50) NOT NULL,
  `cc_detail` varchar(500) NOT NULL,
  `compositon` varchar(50) NOT NULL,
  `presentation` varchar(50) NOT NULL,
  `nie` varchar(50) NOT NULL,
  `dimension` varchar(50) NOT NULL,
  `packaging_id` int(10) NOT NULL,
  `item_number` int(10) NOT NULL,
  `for_by` varchar(50) NOT NULL,
  `distributed_by` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `specialist_id` int(10) NOT NULL,
  `drafter_id` int(10) NOT NULL,
  `corrector_id` int(10) NOT NULL,
  `time_create` datetime NOT NULL,
  `time_update` datetime DEFAULT NULL,
  `time_done` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `job_order`
--

INSERT INTO `job_order` (`jo_number`, `artwork_status`, `input_date`, `due_date`, `prod_name`, `generic_name`, `drug_id`, `dosage_form`, `roa`, `storage`, `manufactured`, `import`, `license`, `marketed`, `cc_number`, `cc_detail`, `compositon`, `presentation`, `nie`, `dimension`, `packaging_id`, `item_number`, `for_by`, `distributed_by`, `material`, `specialist_id`, `drafter_id`, `corrector_id`, `time_create`, `time_update`, `time_done`) VALUES
(1, 'regis,new', '2022-10-22', '2022-11-22', 'product name', 'generic name', 2, 'dosage form', 'roa', 'storage', 'manuf', 'import', 'license', 'market', 'cc number', 'cc detail', 'composition', 'persentation', 'nie', 'dimension', 4, 123, 'for', 'distributed', 'material', 1, 1, 2, '2022-08-18 13:19:30', '2022-08-19 04:10:55', NULL),
(2, '', '2022-11-22', '2022-12-22', 'product name2', 'generic name2', 2, 'dosage form2', 'roa2', 'storage2', 'manuf2', 'import2', 'license2', 'market2', 'cc2', 'cc2', 'comp2', 'per2', 'nie2', 'dimension2', 3, 123, 'for2', 'distributed2', 'material2', 1, 2, 1, '2022-08-18 13:26:38', '2022-08-19 04:08:03', '2022-08-19 04:15:21'),
(3, '', '2022-11-22', '2022-12-22', 'product name2', 'generic name2', 2, 'dosage form2', 'roa2', 'storage2', 'manuf2', 'import2', 'license2', 'market2', 'cc2', 'cc2', 'comp2', 'per2', 'nie2', 'dimension2', 3, 123, 'for2', 'distributed2', 'material2', 1, 2, 1, '2022-08-18 13:31:24', '2022-08-19 04:13:03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `notification`
--

CREATE TABLE `notification` (
  `job_id` int(11) NOT NULL,
  `proses_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `notification`
--

INSERT INTO `notification` (`job_id`, `proses_id`, `notification_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(2, 2, 2),
(2, 2, 2),
(3, 1, 1),
(3, 2, 2),
(1, 1, 3),
(2, 1, 1),
(3, 1, 1),
(3, 2, 2),
(2, 2, 2),
(1, 2, 2),
(3, 1, 1),
(2, 3, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `packaging`
--

CREATE TABLE `packaging` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `packaging`
--

INSERT INTO `packaging` (`id`, `name`) VALUES
(1, 'Doos'),
(2, 'Label'),
(3, 'Poly'),
(4, 'Alufoil'),
(5, 'Alutube'),
(6, 'Leaflet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `proses`
--

CREATE TABLE `proses` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `process_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `proses`
--

INSERT INTO `proses` (`id`, `job_id`, `process_id`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Specialist'),
(2, 'Corrector'),
(3, 'Designer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `specialist`
--

CREATE TABLE `specialist` (
  `id` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(25) NOT NULL,
  `join_date` date NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `specialist`
--

INSERT INTO `specialist` (`id`, `nik`, `full_name`, `username`, `join_date`, `password`) VALUES
(1, 'SP00012345', 'Gina Sonya', 'gmo', '2019-05-01', 'spesialis');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artwork_draft`
--
ALTER TABLE `artwork_draft`
  ADD KEY `draft_drafter_id` (`drafter_id`),
  ADD KEY `draft_job_id` (`job_id`);

--
-- Indeks untuk tabel `artwork_final`
--
ALTER TABLE `artwork_final`
  ADD KEY `final_job_id` (`job_id`),
  ADD KEY `final_corrector_id` (`corrector_id`);

--
-- Indeks untuk tabel `attachment`
--
ALTER TABLE `attachment`
  ADD KEY `attachment_specialist_id` (`specialist_id`),
  ADD KEY `attach_job_id` (`job_id`);

--
-- Indeks untuk tabel `corrector`
--
ALTER TABLE `corrector`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `corrector_comment`
--
ALTER TABLE `corrector_comment`
  ADD KEY `jo_number` (`jo_number`),
  ADD KEY `comment_corrector_id` (`corrector_id`);

--
-- Indeks untuk tabel `drafter`
--
ALTER TABLE `drafter`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `drug`
--
ALTER TABLE `drug`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `job_order`
--
ALTER TABLE `job_order`
  ADD PRIMARY KEY (`jo_number`),
  ADD KEY `specialist_id` (`specialist_id`),
  ADD KEY `corrector_id` (`corrector_id`),
  ADD KEY `drafter_id` (`drafter_id`),
  ADD KEY `drug_id` (`drug_id`),
  ADD KEY `packaging_id` (`packaging_id`);

--
-- Indeks untuk tabel `notification`
--
ALTER TABLE `notification`
  ADD KEY `proses_id` (`proses_id`),
  ADD KEY `job_id` (`job_id`);

--
-- Indeks untuk tabel `packaging`
--
ALTER TABLE `packaging`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `proses`
--
ALTER TABLE `proses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proses_job_id` (`job_id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `specialist`
--
ALTER TABLE `specialist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `corrector`
--
ALTER TABLE `corrector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `drafter`
--
ALTER TABLE `drafter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `drug`
--
ALTER TABLE `drug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `job_order`
--
ALTER TABLE `job_order`
  MODIFY `jo_number` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `packaging`
--
ALTER TABLE `packaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `proses`
--
ALTER TABLE `proses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `specialist`
--
ALTER TABLE `specialist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `artwork_draft`
--
ALTER TABLE `artwork_draft`
  ADD CONSTRAINT `draft_drafter_id` FOREIGN KEY (`drafter_id`) REFERENCES `drafter` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `draft_job_id` FOREIGN KEY (`job_id`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `artwork_final`
--
ALTER TABLE `artwork_final`
  ADD CONSTRAINT `final_corrector_id` FOREIGN KEY (`corrector_id`) REFERENCES `corrector` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `final_job_id` FOREIGN KEY (`job_id`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attach_job_id` FOREIGN KEY (`job_id`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE,
  ADD CONSTRAINT `attachment_specialist_id` FOREIGN KEY (`specialist_id`) REFERENCES `specialist` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `corrector_comment`
--
ALTER TABLE `corrector_comment`
  ADD CONSTRAINT `comment_corrector_id` FOREIGN KEY (`corrector_id`) REFERENCES `corrector` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `corrector_comment_ibfk_1` FOREIGN KEY (`jo_number`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `job_order`
--
ALTER TABLE `job_order`
  ADD CONSTRAINT `corrector_id` FOREIGN KEY (`corrector_id`) REFERENCES `corrector` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `drafter_id` FOREIGN KEY (`drafter_id`) REFERENCES `drafter` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `drug_id` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `packaging_id` FOREIGN KEY (`packaging_id`) REFERENCES `packaging` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `specialist_id` FOREIGN KEY (`specialist_id`) REFERENCES `specialist` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notif_job_id` FOREIGN KEY (`job_id`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE,
  ADD CONSTRAINT `notif_proses_id` FOREIGN KEY (`proses_id`) REFERENCES `proses` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `proses`
--
ALTER TABLE `proses`
  ADD CONSTRAINT `proses_job_id` FOREIGN KEY (`job_id`) REFERENCES `job_order` (`jo_number`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
