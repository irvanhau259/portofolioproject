<?php

include_once("../function/koneksi.php");
include_once("../function/helper.php");
date_default_timezone_set("Asia/Jakarta");

session_start();
$job_order_id = $_GET['job_id'];
$time_done = date('Y-m-d H:i:s');
$time_update = date('Y-m-d H:i:s');
$corrector_id = $_SESSION['id'];
$button = $_POST['button'];
$proses_done = 3;
$proses_update = 1;
$notification_done = 3;
$notification_update = 1;
$comment = $_POST['comment'];
// $attachment = $_POST['attachment'];
$update_gambar = "";

if (!empty($_FILES["artwork_final"]["name"])) {
    $nama_attachment = $_FILES["artwork_final"]["name"];
    move_uploaded_file($_FILES["artwork_final"]["tmp_name"], "../images/artwork-final/" . $nama_attachment);

    $update_gambar = ", gambar='$nama_attachment'";
}

if ($button == "Done") {
    // $query = mysqli_query($koneksi, 
    $query = "UPDATE job_order SET time_done = '$time_done' WHERE jo_number = '$job_order_id'";
    // );
    if ($query) {
        $draft =  "INSERT into artwork_final(job_id,corrector_id,artwork_final)values('$job_order_id','$corrector_id','$nama_attachment')";
        $update_proses = "UPDATE proses set process_id = '$proses_done' where job_id='$job_order_id'";
        $notif = "INSERT into notification(job_id,proses_id,notification_id)values('$job_order_id','$proses_done','$notification_done')";

        if (mysqli_query($koneksi, $query)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $draft)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $draft . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $update_proses)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $update_proses . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $notif)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $notif . "<br>" . mysqli_error($koneksi);
        }
    }
} else if ($button = "Submit") {
    $query  = "UPDATE job_order set time_update = '$time_update' where jo_number = '$job_order_id'";

    if ($query) {
        $update_proses = "UPDATE proses set process_id = '$proses_update' where job_id='$job_order_id'";
        $notif = "INSERT into notification(job_id,proses_id,notification_id)values('$job_order_id','$proses_update','$notification_update')";
        $comment = "INSERT into corrector_comment(jo_number,corrector_id,comment)values('$job_order_id','$corrector_id','$comment')";

        if (mysqli_query($koneksi, $query)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $comment)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $draft . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $update_proses)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $update_proses . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $notif)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $notif . "<br>" . mysqli_error($koneksi);
        }
    }
}


// header("location:" . BASE_URL2 . "dashboard.php?id='$id'");
