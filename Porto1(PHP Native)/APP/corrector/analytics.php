<?php

?>

<?php

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <title>Analytics</title>
</head>

<body>

    <div class="main-container d-flex">
        <?php include("sidebar.php") ?>
        <div class="content">
            <div class="col-md-12 py-3 px-3">
                <div class="container-fluid px-1">
                    <div class="row">

                        <div class="col-md-4 py-3 px-3">
                            <div class="card">
                                <div class="card-header">
                                    Specialist Analytics
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="title">
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>JO Total</th>
                                            </tr>
                                            <?php
                                            $querySpecialist = mysqli_query($koneksi, "SELECT * from specialist ");
                                            $no = 1;
                                            while ($row = mysqli_fetch_assoc($querySpecialist)) {
                                                echo "<tbody class='isi'>
                                                <td>$no</td>
                                                <td>$row[username]</td>";
                                                $queryUser = mysqli_query($koneksi, "SELECT * from job_order where job_order.specialist_id='$row[id]'");
                                                $countUser = mysqli_num_rows($queryUser);
                                                echo "<td>$countUser</td>";
                                                echo "</tbody>";
                                                $no++;
                                            }

                                            ?>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 py-3 px-3">
                            <div class="card">
                                <div class="card-header">
                                    Designer Analytics
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="title">
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>JO Total</th>
                                            </tr>
                                            <?php
                                            $queryDrafter = mysqli_query($koneksi, "SELECT * from drafter ");
                                            $no = 1;
                                            while ($row = mysqli_fetch_assoc($queryDrafter)) {
                                                echo "<tbody class='isi'>
                                                <td>$no</td>
                                                <td>$row[username]</td>";
                                                $queryUser = mysqli_query($koneksi, "SELECT * from job_order where job_order.drafter_id='$row[id]'");
                                                $countUser = mysqli_num_rows($queryUser);
                                                echo "<td>$countUser</td>";
                                                echo "</tbody>";
                                                $no++;
                                            }

                                            ?>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 py-3 px-3">
                            <div class="card">
                                <div class="card-header">
                                    Corrector Analytics
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr class="title">
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>JO Total</th>
                                            </tr>
                                            <?php
                                            $queryCorrector = mysqli_query($koneksi, "SELECT * from corrector ");
                                            $no = 1;
                                            while ($row = mysqli_fetch_assoc($queryCorrector)) {
                                                echo "<tbody class='isi'>
                                                <td>$no</td>
                                                <td>$row[username]</td>";
                                                $queryUser = mysqli_query($koneksi, "SELECT * from job_order where job_order.corrector_id='$row[id]'");
                                                $countUser = mysqli_num_rows($queryUser);
                                                echo "<td>$countUser</td>";
                                                echo "</tbody>";
                                                $no++;
                                            }

                                            ?>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="../assets/jquery-3.6.0.min.js"></script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->

</body>

</html>