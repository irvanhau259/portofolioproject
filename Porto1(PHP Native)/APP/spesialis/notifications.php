<?php
include_once("../function/helper.php");
include_once("../function/koneksi.php");
date_default_timezone_set("Asia/Jakarta");

$notification_text = [
  "",
  "Sedang di Drafting",
  "Sedang di Check",
  "Sudah Selesai"
];

?>

<?php

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <title>Notification</title>
</head>

<body>

  <div class="main-container d-flex">
    <?php include("sidebar.php") ?>
    <div class="content">
      <div class="col-md-12 py-3 px-3">
        <?php
        $queryCheck = mysqli_query($koneksi, "SELECT job_order.*,notification.notification_id from job_order join notification on job_order.jo_number=notification.job_id where job_order.specialist_id='$id' order by time_done desc,time_update desc,time_create desc, notification_id desc");
        while ($rowJob = mysqli_fetch_assoc($queryCheck)) {
          $time_create = strtotime($rowJob['time_create']);
          $time_update = strtotime($rowJob['time_update']);
          $time_done = strtotime($rowJob['time_done']);
          $minute_now = strtotime("now");
          // waktu notif
          $create_notif =  $minute_now - $time_create;
          $update_notif = $minute_now - $time_update;
          $done_notif = $minute_now - $time_done;
          // time create
          $hariDraft = floor($create_notif / (60 * 60 * 24));
          $jamDraft   = floor($create_notif / (60 * 60));
          $menitDraft = floor(($create_notif) / 60);
          // time update
          $hariCheck = floor($update_notif / (60 * 60 * 24));
          $jamCheck   = floor($update_notif / (60 * 60));
          $menitCheck = floor(($update_notif) / 60);
          // time done
          $hariDone = floor($done_notif / (60 * 60 * 24));
          $jamDone   = floor($done_notif / (60 * 60));
          $menitDone = floor(($done_notif) / 60);
          // get notif id 
          $notification_id = $rowJob['notification_id'];
        ?>
          <div class="card my-2
          ">
            <div class="card-body">
              <p class="card-text"><small class="text-muted">
                  <?php
                  if ($notification_id == 1) {
                    if ($menitDraft > 1439) {
                      echo $hariDraft . " Days Ago";
                    } else if ($menitDraft > 59) {
                      echo $jamDraft . " Hours Ago";
                    } else if ($menitDraft > 0) {
                      echo $menitDraft . " Minutes Ago";
                    } else {
                      echo "Just Now";
                    }
                  } else if ($notification_id == 2) {
                    if ($menitCheck > 1439) {
                      echo $hariCheck . " Days Ago";
                    } else if ($menitCheck > 59) {
                      echo $jamCheck . " Hours Ago";
                    } else if ($menitCheck > 0) {
                      echo $menitCheck . " Minutes Ago";
                    } else {
                      echo "Just Now";
                    }
                  } else if ($notification_id == 3) {
                    if ($menitDone > 1439) {
                      echo $hariDone . " Days Ago";
                    } else if ($menitDone > 59) {
                      echo $jamDone . " Hours Ago";
                    } else if ($menitDone > 0) {
                      echo $menitDone . " Minutes Ago";
                    } else {
                      echo "Just Now";
                    }
                  }
                  ?>
                </small></p>
              <p class="card-text"><?php echo "Job Number $rowJob[jo_number] " . $notification_text[$notification_id]; ?></p>
              <a href="<?php echo BASE_URL1 . "detail.php?job_id=$rowJob[jo_number]"; ?>" class="card-link text-decoration-none">
                <p class="card-text">See More >>
                </p>
              </a>
            </div>

          </div>
        <?php } ?>
      </div>
    </div>
  </div>




  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script src="../assets/jquery-3.6.0.min.js"></script>
  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->

</body>

</html>