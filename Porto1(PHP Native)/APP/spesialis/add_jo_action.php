<?php

include_once("../function/koneksi.php");
include_once("../function/helper.php");
date_default_timezone_set("Asia/Jakarta");

$artwork = implode(",", $_POST['artwork']);
$input_date = date('Y-m-d', strtotime($_POST['input_date']));
$due_date = date('Y-m-d', strtotime($_POST['due_date']));
$prod_name = $_POST['prod_name'];
$generic_name = $_POST['generic_name'];
$drug_id = $_POST['drug_id'];
$dosage_form = $_POST['dosage_form'];
$roa = $_POST['roa'];
$storage = $_POST['storage'];
$manufactured = $_POST['manufactured'];
$for = $_POST['for'];
$marketed = $_POST['marketed'];
$imported = $_POST['imported'];
$license = $_POST['license'];
$distributed = $_POST['distributed'];
$cc_number = $_POST['cc_number'];
$cc_detail = $_POST['cc_detail'];
$composition = $_POST['composition'];
$persentation = $_POST['persentation'];
$nie = $_POST['nie'];
$packaging_id = $_POST['packaging_id'];
$no_item = $_POST['no_item'];
$dimension = $_POST['dimension'];
$material = $_POST['material'];
$spesialis_id = $_POST['spesialis_id'];
$corrector_id = $_POST['corrector_id'];
$drafter_id = $_POST['drafter_id'];
$time_create = date('Y-m-d H:i:s');
$button = $_POST['button'];
$proses = 1;
$notification_id = 1;
// $attachment = $_POST['attachment'];
$update_gambar = "";

if (!empty($_FILES["attachment"]["name"])) {
    $nama_attachment = $_FILES["attachment"]["name"];
    move_uploaded_file($_FILES["attachment"]["tmp_name"], "../images/attachment/" . $nama_attachment);

    $update_gambar = ", gambar='$nama_attachment'";
}

if ($button == "Submit") {
    $query = mysqli_query($koneksi, "INSERT into job_order (
            artwork_status,
            input_date,
            due_date,
            prod_name,
            generic_name,
            drug_id,
            dosage_form,
            roa,
            storage,
            manufactured,
            import,
            license,
            marketed,
            cc_number,
            cc_detail,
            compositon,
            presentation,
            nie,
            dimension,
            packaging_id,
            item_number,
            for_by,
            distributed_by,
            material,
            specialist_id,
            drafter_id, 
            corrector_id,
            time_create)
            values (
        '$artwork',
        '$input_date',
        '$due_date',
        '$prod_name',
        '$generic_name',
        '$drug_id',
        '$dosage_form',
        '$roa',
        '$storage',
        '$manufactured',
        '$imported',
        '$license',
        '$marketed',
        '$cc_number',
        '$cc_detail',
        '$composition',
        '$persentation',
        '$nie',
        '$dimension',
        '$packaging_id',
        '$no_item',
        '$for',
        '$distributed',
        '$material',
        '$spesialis_id',
        '$drafter_id',
        '$corrector_id',
        '$time_create')");
    if ($query) {
        $job_id = mysqli_insert_id($koneksi);
        mysqli_query($koneksi, "INSERT into attachment(job_id,specialist_id,attachment)values('$job_id','$spesialis_id','$nama_attachment')");
        mysqli_query($koneksi, "INSERT INTO proses(job_id,process_id)values('$job_id','$proses')");
        mysqli_query($koneksi, "INSERT into notification(job_id,proses_id,notification_id)values('$job_id','$proses','$notification_id')");
    }
}

header("location:" . BASE_URL1 . "dashboard.php?id='$id'");
// if (mysqli_query($koneksi, $process)) {
//     echo "New record created successfully";
// } else {
//     echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
// }
// mysqli_close($koneksi);
// if (mysqli_query($koneksi, $attachment)) {
//     echo "New record created successfully";
// } else {
    // echo "Error: " . $attachment . "<br>" . mysqli_error($koneksi);
// }
// mysqli_close($koneksi);
