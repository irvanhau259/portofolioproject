<?php
include_once("../function/helper.php");
include_once("../function/koneksi.php");

$job_id = $_GET['job_id'];

$queryJob = mysqli_query($koneksi, "SELECT job_order.*,specialist.username,proses.process_id from job_order join specialist on job_order.specialist_id=specialist.id join proses on proses.job_id=job_order.jo_number where job_order.jo_number = '$job_id'");
$queryAttachment = mysqli_query($koneksi, "SELECT attachment.* from attachment join job_order on attachment.job_id=job_order.jo_number");
$queryDraft = mysqli_query($koneksi, "SELECT artwork_draft.* from artwork_draft join job_order on artwork_draft.job_id=job_order.jo_number where artwork_draft.job_id=$job_id");
$queryFinal = mysqli_query($koneksi, "SELECT artwork_final.* from artwork_final join job_order on artwork_final.job_id=job_order.jo_number where artwork_final.job_id=$job_id");

$row = mysqli_fetch_assoc($queryJob);
$rowAttach = mysqli_fetch_assoc($queryAttachment);
$rowDraft = mysqli_fetch_assoc($queryDraft);
$rowFinal = mysqli_fetch_assoc($queryFinal);

$jo_number = $row['jo_number'];
$artwork = explode(',', $row['artwork_status']);
$input_date = date('d-m-Y', strtotime($row['input_date']));
$due_date = date('d-m-Y', strtotime($row['due_date']));
$prod_name = $row['prod_name'];
$generic_name = $row['generic_name'];
$drug_id = $row['drug_id'];
$dosage_form = $row['dosage_form'];
$roa = $row['roa'];
$storage = $row['storage'];
$manufactured = $row['manufactured'];
$for = $row['for_by'];
$marketed = $row['marketed'];
$imported = $row['import'];
$license = $row['license'];
$distributed = $row['distributed_by'];
$cc_number = $row['cc_number'];
$cc_detail = $row['cc_detail'];
$composition = $row['compositon'];
$persentation = $row['presentation'];
$nie = $row['nie'];
$packaging_id = $row['packaging_id'];
$no_item = $row['item_number'];
$dimension = $row['dimension'];
$material = $row['material'];
$spesialis_id = $row['specialist_id'];
$corrector_id = $row['corrector_id'];
$drafter_id = $row['drafter_id'];
$attachment = "<img src='" . BASE_URL . "images/attachment/$rowAttach[attachment]' style='width: 200px;vertical-align: middle;' />";
$artwork_draft = "";
$artwork_final = "";
?>

<?php

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <style>
        .form-label {
            font-weight: bold;
            margin-top: 5px;
        }
    </style>
    <title>Detail Job Order</title>
</head>

<body>

    <div class="main-container d-flex">
        <?php include("sidebar.php") ?>
        <div class="content">
            <div class="col-md-12 py-3 px-3">
                <div class="row">
                    <div class="col">
                        <label for="JONumber" class="form-label">JO Number</label>
                        <input type="text" class="form-control" value="<?php echo $jo_number; ?>" aria-label="JONumber" disabled>
                    </div>
                    <div class="col py-4">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="artwork[]" type="checkbox" id="inlineCheckbox1" value="regis" <?php if (in_array("regis", $artwork)) echo "checked"; ?> disabled>
                            <label class="form-check-label" for="inlineCheckbox1">Registrasi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="artwork[]" type="checkbox" id="inlineCheckbox2" value="launching" <?php if (in_array("launching", $artwork)) echo "checked"; ?> disabled>
                            <label class="form-check-label" for="inlineCheckbox2">Launching</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="artwork[]" type="checkbox" id="inlineCheckbox3" value="new" <?php if (in_array("new", $artwork)) echo "checked"; ?> disabled>
                            <label class="form-check-label" for="inlineCheckbox3">New</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="artwork[]" type="checkbox" id="inlineCheckbox4" value="change" <?php if (in_array("change", $artwork)) echo "checked"; ?> disabled>
                            <label class="form-check-label" for="inlineCheckbox4">Change</label>
                        </div>
                        <a href="<?php echo BASE_URL1 . "dashboard.php?id=$id"; ?>"><button class="btn btn-warning" type="button">Back</button></a>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="formFile" class="form-label">Input Date</label>
                        <input type="text" class="form-control" value="<?php echo $input_date; ?>" aria-label="First name" disabled>
                    </div>
                    <div class="col">
                        <label for="formFile" class="form-label">Due Date</label>
                        <input type="text" class="form-control" value="<?php echo $due_date; ?>" aria-label="Last name" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="ProductName" class="form-label">Product Name</label>
                        <input type="text" class="form-control" value="<?php echo $prod_name; ?>" id="ProductName" aria-label="ProductName" disabled>
                    </div>
                    <div class="col">
                        <label for="GenericName" class="form-label">Generic Name</label>
                        <input type="text" class="form-control" value="<?php echo $generic_name; ?>" id="GenericName" aria-label="GenericName" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="DrugCategory" class="form-label">Drug Category</label>
                        <select class="form-select mb-3" aria-label=".form-select-lg example" disabled>
                            <option selected disabled>--- Drug Category ---</option>
                            <?php
                            $query = mysqli_query($koneksi, "SELECT id,name FROM drug");
                            while ($row = mysqli_fetch_assoc($query)) {
                                if ($drug_id == $row['id']) {
                                    echo "<option value='$row[id]' selected='true'>$row[name]</option>";
                                } else {
                                    echo "<option value='$row[id]'>$row[name]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col">
                        <label for="DossageForm" class="form-label">Dossage Form</label>
                        <input type="text" class="form-control" value="<?php echo $dosage_form; ?>" id="DossageForm" aria-label="DossageForm" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="ROA" class="form-label">Route Of Administration</label>
                        <input type="text" class="form-control" value="<?php echo $roa; ?>" id="ROA" aria-label="ROA" disabled>
                    </div>
                    <div class="col">
                        <label for="Storage" class="form-label">Storage</label>
                        <input type="text" class="form-control" value="<?php echo $storage; ?>" id="Storage" aria-label="Storage" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="mfg" class="form-label">Manufactured By</label>
                        <input type="text" class="form-control" value="<?php echo $manufactured; ?>" id="mfg" aria-label="mfg" disabled>
                    </div>
                    <div class="col">
                        <label for="for" class="form-label">For</label>
                        <input type="text" class="form-control" value="<?php echo $for; ?>" id="for" aria-label="for" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="mkt" class="form-label">Marketed By</label>
                        <input type="text" class="form-control" value="<?php echo $marketed; ?>" id="mkt" aria-label="mkt" disabled>
                    </div>
                    <div class="col">
                        <label for="imported" class="form-label">Imported By</label>
                        <input type="text" class="form-control" value="<?php echo $imported; ?>" id="imported" aria-label="imported" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="licensed" class="form-label">Under License</label>
                        <input type="text" class="form-control" value="<?php echo $license; ?>" id="license" aria-label="license" disabled>
                    </div>
                    <div class="col">
                        <label for="distributed" class="form-label">Distributed By</label>
                        <input type="text" class="form-control" value="<?php echo $distributed; ?>" id="distributed" aria-label="distributed" disabled>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="ccNumber" class="form-label">No Change Control</label>
                        <textarea class="form-control" id="ccNumber" rows="3" disabled><?php echo $cc_number; ?></textarea>
                    </div>
                    <div class="col">
                        <label for="ccDetail" class="form-label">Change Control Detail</label>
                        <textarea class="form-control" id="ccDetail" rows="3" disabled><?php echo $cc_detail; ?></textarea>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="composition" class="form-label">Composition</label>
                        <textarea class="form-control" id="composition" rows="3" disabled><?php echo $composition; ?></textarea>
                    </div>
                    <div class="col">
                        <label for="persentation" class="form-label">Persentation</label>
                        <textarea class="form-control" id="persentation" rows="3" disabled><?php echo $composition; ?></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="nie" class="form-label">NIE</label>
                        <input type="text" class="form-control" value="<?php echo $nie; ?>" id="nie" aria-label="nie" disabled>
                    </div>
                    <div class="col">
                        <label for="distributed" class="form-label">Distributed By</label>
                        <select class="form-select mb-3" aria-label=".form-select-lg example" disabled>
                            <option selected disabled>--- Packaging ---</option>
                            <?php
                            $query = mysqli_query($koneksi, "SELECT id,name FROM packaging");
                            while ($row = mysqli_fetch_assoc($query)) {
                                if ($packaging_id == $row['id']) {
                                    echo "<option value='$row[id]' selected='true'>$row[name]</option>";
                                } else {
                                    echo "<option value='$row[id]'>$row[name]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <label for="NoItem" class="form-label">No Item</label>
                        <input type="text" class="form-control" value="<?php echo $no_item; ?>" id="NoItem" aria-label="NoItem" disabled>
                    </div>
                    <div class="col">
                        <label for="dimension" class="form-label">Dimension</label>
                        <input type="text" class="form-control" value="<?php echo $dimension; ?>" id="dimension" aria-label="dimension" disabled>
                    </div>
                    <div class="col">
                        <label for="material" class="form-label">Material</label>
                        <input type="text" class="form-control" value="<?php echo $material; ?>" id="material" aria-label="material" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <label for="specialist" class="form-label">Specialist</label>
                        <select class="form-select mb-3" aria-label=".form-select-lg example" disabled>
                            <?php
                            echo "<option disabled selected='true'>--- Pilih Specialist ---</option>";
                            $querySpesialis = mysqli_query($koneksi, "SELECT * from specialist ");
                            while ($row = mysqli_fetch_assoc($querySpesialis)) {
                                if ($spesialis_id == $row['id']) {
                                    echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                } else {
                                    echo "<option value='$row[id]'>$row[username]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col">
                        <label for="corrector" class="form-label">Corrector</label>
                        <select class="form-select mb-3" aria-label=".form-select-lg example" disabled>
                            <?php
                            echo "<option disabled selected='true'>---  Pilih Corrector ---</option>";
                            $queryCorector = mysqli_query($koneksi, "SELECT * from corrector");
                            while ($row = mysqli_fetch_assoc($queryCorector)) {
                                if ($corrector_id == $row['id']) {
                                    echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                } else {
                                    echo "<option value='$row[id]'>$row[username]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col">
                        <label for="drafter" class="form-label">Drafter</label>
                        <select class="form-select mb-3" aria-label=".form-select-lg example" disabled>
                            <?php
                            echo "<option disabled selected='true'>---  Pilih Drafter   ---</option>";
                            $queryDrafter = mysqli_query($koneksi, "SELECT * from drafter");
                            while ($row = mysqli_fetch_assoc($queryDrafter)) {
                                if ($drafter_id == $row['id']) {
                                    echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                } else {
                                    echo "<option value='$row[id]'>$row[username]</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col">
                    <label for="Attachment" class="form-label">Attachment</label>
                    <input type="text" class="form-control mb-2" id="inputGroupFile01" value="<?php echo $rowAttach['attachment']; ?>" disabled>
                    <a href="<?php echo BASE_URL1 . "download.php?file=$rowAttach[attachment]"; ?>"><button type="submit" class="btn btn-primary">Download Attachment</button></a>
                </div>
                <div class=" col">
                    <label for="ArtwrokDraft" class="form-label">Artwrok Draft</label>
                    <?php
                    if (isset($rowDraft)) {
                        echo "<input type='text' class='form-control mb-2' name='artwork_draft' value='$rowDraft[artwork_draft]' disabled>";
                        echo "<a href='" . BASE_URL2 . "download.php?file=$rowDraft[artwork_draft]'><button type='submit' class='btn btn-primary'>Download Attachment</button></a>";
                        // echo "<button type='submit' class='btn btn-primary'>Download Artwrok Draft</button>";
                    } else if (isset($rowDraft)) {
                        echo "
                                <input type='file' class='form-control mb-2' disabled name='artwork_draft'>
                                <button type='submit' class='btn btn-primary' disabled>Download Artwrok Final</button>
                                ";
                    }
                    ?>
                </div>
                <div class="col">
                    <label for="ArtwrokFinal" class="form-label">Artwrok Final</label>
                    <?php
                    if (isset($rowFinal)) {
                        echo "<input type='text' class='form-control mb-2' name='artwork_final' value='$rowFinal[artwork_final]' disabled>";
                        echo "<a href='" . BASE_URL3
                            . "download.php?file=$rowFinal[artwork_final]'><button type='submit' class='btn btn-primary'>Download Attachment</button></a>";
                        // echo "<button type='submit' class='btn btn-primary'>Download Artwrok Draft</button>";
                    } else {
                        echo "
                                <input type='file' class='form-control mb-2' disabled name='artwork_final'>
                                <button type='submit' class='btn btn-primary' disabled>Download Artwrok Final</button>
                                ";
                    }
                    ?>
                </div>



                <div class="my-3">
                    <a href="<?php echo BASE_URL1 . "dashboard.php?id=$id"; ?>"><button class="btn btn-warning" type="button">Back</button></a>
                </div>

            </div>
        </div>
    </div>




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="../assets/jquery-3.6.0.min.js"></script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->

</body>

</html>