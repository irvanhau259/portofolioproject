<?php

include_once("../function/helper.php");
include_once("../function/koneksi.php");

session_start();
$id = $_SESSION['id'];

$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : "";
$nik = isset($_POST["nip"]) && isset($_POST['kode_role']) ? $_POST['kode_role'] . $_POST['nip'] : null;
$role = isset($_POST['kode_role']) ? $_POST['kode_role'] : $_GET['role'];
$fullname = isset($_POST['fullname']) ? $_POST['fullname'] : "";
$join_date = isset($_POST['join_date']) ? date('Y-m-d', strtotime($_POST['join_date'])) : "";
$username = isset($_POST['username']) ? $_POST['username'] : "";
$password = isset($_POST['password']) ? $_POST['password'] : "";

$button = isset($_POST['button']) ? $_POST['button'] : $_GET['button'];

if ($button == "Save") {
    if ($role == "SP") {
        mysqli_query($koneksi, "INSERT into specialist (nik,full_name,join_date,username,password)values('$nik','$fullname','$join_date','$username','$password')");
    } else if ($role == "DR") {
        mysqli_query($koneksi, "INSERT into drafter (nik,full_name,join_date,username,password)values('$nik','$fullname','$join_date','$username','$password')");
    } else if ($role == "CR") {
        mysqli_query($koneksi, "INSERT into corrector (nik,full_name,join_date,username,password)values('$nik','$fullname','$join_date','$username','$password')");
    }
} elseif ($button == "Update") {
    if ($role == "SP") {
        mysqli_query($koneksi, "UPDATE specialist set nik = '$nik',full_name='$fullname',join_date='$join_date',username='$username',password='$password' where id = '$user_id'");
    } else if ($role == "DR") {
        mysqli_query($koneksi, "UPDATE drafter set nik = '$nik',full_name='$fullname',join_date='$join_date',username='$username',password='$password' where id = '$user_id'");
    } else if ($role == "CR") {
        mysqli_query($koneksi, "UPDATE corrector set nik = '$nik',full_name='$fullname',join_date='$join_date',username='$username',password='$password' where id = '$user_id'");
    }
} elseif ($button == "Delete") {
    if ($role == "SP") {
        mysqli_query($koneksi, "DELETE from specialist where id='$user_id'");
    } else if ($role == "DR") {
        mysqli_query($koneksi, "DELETE from drafter where id='$user_id'");
    } else if ($role == "CR") {
        mysqli_query($koneksi, "DELETE from corrector where id='$user_id'");
    }
}

// header("location:" . BASE_URL1 . "user_management.php?id=$id");
