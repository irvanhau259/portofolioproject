<?php
include_once("../function/helper.php");
include_once("../function/koneksi.php");

$id = isset($_GET['id']) ? $_GET['id'] : "";

$jo_number = "";
$artwork = "";
$input_date = "";
$due_date = "";
$prod_name = "";
$generic_name = "";
$drug_id = "";
$dosage_form = "";
$roa = "";
$storage = "";
$manufactured = "";
$for = "";
$marketed = "";
$imported = "";
$license = "";
$distributed = "";
$cc_number = "";
$cc_detail = "";
$composition = "";
$persentation = "";
$nie = "";
$packaging_id = "";
$no_item = "";
$dimension = "";
$material = "";
$spesialis_id = "";
$corrector_id = "";
$drafter_id = "";
$attachment = "";
$artwork_draft = "";
$artwork_final = "";
$button = "Submit";
?>

<?php

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <style>
        .form-label {
            font-weight: bold;
            margin-top: 5px;
        }
    </style>
    <title>Add Job Order</title>
</head>

<body>

    <div class="main-container d-flex">
        <?php include("sidebar.php") ?>
        <div class="content">
            <div class="col-md-12 py-3 px-3">
                <form action="<?php echo BASE_URL1 . "add_jo_action.php"; ?>" method="POST" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col">
                            <label for="JONumber" class="form-label">JO Number</label>
                            <input type="text" class="form-control" disabled name="jo_number" value="<?php echo "$jo_number"; ?>" aria-label="JONumber">
                        </div>
                        <div class="col py-4">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="artwork[]" type="checkbox" id="regis" value="regis" onclick="regis()">
                                <label class="form-check-label" for="inlineCheckbox1">Registrasi</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="artwork[]" type="checkbox" id="launch" value="launching">
                                <label class="form-check-label" for="inlineCheckbox2">Launching</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="artwork[]" type="checkbox" id="new" value="new">
                                <label class="form-check-label" for="inlineCheckbox3">New</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="artwork[]" type="checkbox" id="change" value="change">
                                <label class="form-check-label" for="inlineCheckbox4">Change</label>
                            </div>
                            <a href="<?php echo BASE_URL1 . "dashboard.php?id=$id"; ?>"><button class="btn btn-warning" type="button">Cancel</button></a>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="formFile" class="form-label">Input Date</label>
                            <input type="date" class="form-control" name="input_date" value="<?php echo "$input_date"; ?>" aria-label="First name">
                        </div>
                        <div class="col">
                            <label for="formFile" class="form-label">Due Date</label>
                            <input type="date" name="due_date" class="form-control" value="<?php echo "$due_date"; ?>" aria-label="Last name">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="ProductName" class="form-label">Product Name</label>
                            <input type="text" name="prod_name" class="form-control" value="<?php echo "$prod_name"; ?>" id="ProductName" aria-label="ProductName">
                        </div>
                        <div class="col">
                            <label for="GenericName" class="form-label">Generic Name</label>
                            <input type="text" name="generic_name" class="form-control" value="<?php echo "$generic_name"; ?>" id="GenericName" aria-label="GenericName">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="DrugCategory" class="form-label">Drug Category</label>
                            <select class="form-select mb-3" name="drug_id" aria-label=".form-select-lg example">
                                <option selected disabled>--- Drug Category ---</option>
                                <?php
                                $query = mysqli_query($koneksi, "SELECT id,name FROM drug");
                                while ($row = mysqli_fetch_assoc($query)) {
                                    if ($drug_id == $row['id']) {
                                        echo "<option value='$row[id]' selected='true'>$row[name]</option>";
                                    } else {
                                        echo "<option value='$row[id]'>$row[name]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <label for="DossageForm" class="form-label">Dossage Form</label>
                            <input type="text" name="dosage_form" class="form-control" value="<?php echo $dosage_form; ?>" id="DossageForm" aria-label="DossageForm">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="ROA" class="form-label">Route Of Administration</label>
                            <input type="text" name="roa" class="form-control" value="<?php echo $roa; ?>" id="ROA" aria-label="ROA">
                        </div>
                        <div class="col">
                            <label for="Storage" class="form-label">Storage</label>
                            <input type="text" name="storage" class="form-control" value="<?php echo $storage; ?>" id="Storage" aria-label="Storage">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="mfg" class="form-label">Manufactured By</label>
                            <input type="text" name="manufactured" class="form-control" value="<?php echo $manufactured; ?>" id="mfg" aria-label="mfg">
                        </div>
                        <div class="col">
                            <label for="for" class="form-label">For</label>
                            <input type="text" name="for" class="form-control" value="<?php echo $for; ?>" id="for" aria-label="for">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="mkt" class="form-label">Marketed By</label>
                            <input type="text" name="marketed" class="form-control" value="<?php echo $marketed; ?>" id="mkt" aria-label="mkt">
                        </div>
                        <div class="col">
                            <label for="imported" class="form-label">Imported By</label>
                            <input type="text" name="imported" class="form-control" value="<?php echo $imported; ?>" id="imported" aria-label="imported">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="licensed" class="form-label">Under License</label>
                            <input type="text" name="license" class="form-control" value="<?php echo $license; ?>" id="license" aria-label="license">
                        </div>
                        <div class="col">
                            <label for="distributed" class="form-label">Distributed By</label>
                            <input type="text" name="distributed" class="form-control" value="<?php echo $distributed; ?>" id="distributed" aria-label="distributed">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="ccNumber" class="form-label">No Change Control</label>
                            <textarea class="form-control" name="cc_number" id="ccNumber" rows="3"><?php echo $cc_number; ?></textarea>
                        </div>
                        <div class="col">
                            <label for="ccDetail" class="form-label">Change Control Detail</label>
                            <textarea class="form-control" name="cc_detail" id="ccDetail" rows="3"><?php echo $cc_detail; ?></textarea>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="composition" class="form-label">Composition</label>
                            <textarea class="form-control" name="composition" id="composition" rows="3"><?php echo $composition; ?></textarea>
                        </div>
                        <div class="col">
                            <label for="persentation" class="form-label">Persentation</label>
                            <textarea class="form-control" name="persentation" id="persentation" rows="3"><?php echo $persentation; ?></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="nie" class="form-label">NIE</label>
                            <input type="text" name="nie" class="form-control" value="<?php echo $nie; ?>" id="nie" aria-label="nie">
                        </div>
                        <div class="col">
                            <label for="distributed" class="form-label">Packaging By</label>
                            <select class="form-select mb-3" name="packaging_id" aria-label=".form-select-lg example">
                                <option selected disabled>--- Packaging ---</option>
                                <?php
                                $query = mysqli_query($koneksi, "SELECT id,name FROM packaging");
                                while ($row = mysqli_fetch_assoc($query)) {
                                    if ($packaging_id == $row['id']) {
                                        echo "<option value='$row[id]' selected='true'>$row[name]</option>";
                                    } else {
                                        echo "<option value='$row[id]'>$row[name]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col">
                            <label for="NoItem" class="form-label">No Item</label>
                            <input type="text" name="no_item" class="form-control" value="<?php echo $no_item; ?>" id="NoItem" aria-label="NoItem">
                        </div>
                        <div class="col">
                            <label for="dimension" class="form-label">Dimension</label>
                            <input type="text" name="dimension" class="form-control" value="<?php echo $dimension; ?>" id="dimension" aria-label="dimension">
                        </div>
                        <div class="col">
                            <label for="material" class="form-label">Material</label>
                            <input type="text" name="material" class="form-control" value="<?php echo $material; ?>" id="material" aria-label="material">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="specialist" class="form-label">Specialist</label>
                            <select class="form-select mb-3" name="spesialis_id" aria-label=".form-select-lg example">
                                <?php
                                echo "<option disabled selected='true'>--- Pilih Specialist ---</option>";
                                $querySpesialis = mysqli_query($koneksi, "SELECT * from specialist ");
                                while ($row = mysqli_fetch_assoc($querySpesialis)) {
                                    if ($spesialis_id == $row['id']) {
                                        echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                    } else {
                                        echo "<option value='$row[id]'>$row[username]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <label for="corrector" class="form-label">Corrector</label>
                            <select class="form-select mb-3" name="corrector_id" aria-label=".form-select-lg example">
                                <?php
                                echo "<option disabled selected='true'>---  Pilih Corrector ---</option>";
                                $queryCorector = mysqli_query($koneksi, "SELECT * from corrector");
                                while ($row = mysqli_fetch_assoc($queryCorector)) {
                                    if ($corrector_id == $row['id']) {
                                        echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                    } else {
                                        echo "<option value='$row[id]'>$row[username]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col">
                            <label for="drafter" class="form-label">Drafter</label>
                            <select class="form-select mb-3" name="drafter_id" aria-label=".form-select-lg example">
                                <?php
                                echo "<option disabled selected='true'>---  Pilih Drafter   ---</option>";
                                $queryDrafter = mysqli_query($koneksi, "SELECT * from drafter");
                                while ($row = mysqli_fetch_assoc($queryDrafter)) {
                                    if ($drafter_id == $row['id']) {
                                        echo "<option value='$row[id]' selected='true'>$row[username]</option>";
                                    } else {
                                        echo "<option value='$row[id]'>$row[username]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <label for="Attachment" class="form-label">Attachment</label>
                            <input type="file" class="form-control mb-2" name="attachment" id="inputGroupFile01"><?php echo $attachment; ?>
                            <button type="submit" class="btn btn-primary">Download Attachment</button>
                        </div>
                        <div class="col">
                            <label for="ArtwrokDraft" class="form-label">Artwrok Draft</label>
                            <input type="file" class="form-control mb-2" name="artwork_draft" id="inputGroupFile01" disabled><?php echo $artwork_draft; ?>
                            <button type="submit" class="btn btn-primary" disabled>Download Artwrok Draft</button>
                        </div>
                        <div class="col">
                            <label for="ArtwrokFinal" class="form-label">Artwrok Final</label>
                            <input type="file" class="form-control mb-2" name="artwork_final" id="inputGroupFile01" disabled><?php echo $artwork_final; ?>
                            <button type="submit" class="btn btn-primary" disabled>Download Artwrok Final</button>
                        </div>

                    </div>

                    <div class="mb-3">
                        <input type="submit" name="button" value="<?php echo $button; ?>" class="btn btn-danger">
                        <a href="<?php echo BASE_URL1 . "dashboard.php?id=$id"; ?>"><button class="btn btn-warning" type="button">Cancel</button></a>
                    </div>
                </form>

            </div>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="../assets/jquery-3.6.0.min.js"></script>

    <script>
        $("#regis").click(function() {
            if ($(this).is(':checked')) {
                $('#launch').prop('disabled', true)
            } else {
                $('#launch').prop('disabled', false)
            }
        })
        $("#launch").click(function() {
            if ($(this).is(':checked')) {
                $('#regis').prop('disabled', true)
            } else {
                $('#regis').prop('disabled', false)
            }
        })
        $("#new").click(function() {
            if ($(this).is(':checked')) {
                $('#change').prop('disabled', true)
            } else {
                $('#change').prop('disabled', false)
            }
        })
        $("#change").click(function() {
            if ($(this).is(':checked')) {
                $('#new').prop('disabled', true)
            } else {
                $('#new').prop('disabled', false)
            }
        })
    </script>
</body>

</html>