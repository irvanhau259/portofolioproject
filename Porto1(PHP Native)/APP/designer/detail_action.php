<?php

include_once("../function/koneksi.php");
include_once("../function/helper.php");
date_default_timezone_set("Asia/Jakarta");

session_start();
$queryDraft = mysqli_query($koneksi, "SELECT artwork_draft.* from artwork_draft join job_order on artwork_draft.job_id=job_order.jo_number where artwork_draft.job_id=$job_id");
$rowDraft = mysqli_fetch_assoc($queryDraft);

$job_order_id = $_GET['job_id'];
$time_update = date('Y-m-d H:i:s');
$drafter_id = $_SESSION['id'];
$button = $_POST['button'];
$proses = 2;
$notification_id = 2;
// $attachment = $_POST['attachment'];
$update_gambar = "";

if (!empty($_FILES["artwork_draft"]["name"])) {
    $nama_attachment = $_FILES["artwork_draft"]["name"];
    move_uploaded_file($_FILES["artwork_draft"]["tmp_name"], "../images/artwork-draft/" . $nama_attachment);

    $update_gambar = ", gambar='$nama_attachment'";
}

if ($button == "Submit") {
    $query = "UPDATE job_order SET time_update = '$time_update' WHERE jo_number = '$job_order_id'";
    if ($query) {
        if (isset($queryDraft)) {
            $draft =  "UPDATE artwork_draft SET artwork_draft = '$nama_attachment'";
        } else {
            $draft =  "INSERT into artwork_draft(job_id,drafter_id,artwork_draft)values('$job_order_id','$drafter_id','$nama_attachment')";
        }
        $update_proses = "UPDATE proses set process_id = '$proses' where job_id='$job_order_id'";
        $notif = "INSERT into notification(job_id,proses_id,notification_id)values('$job_order_id','$proses','$notification_id')";

        if (mysqli_query($koneksi, $query)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $draft)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $draft . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $update_proses)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $update_proses . "<br>" . mysqli_error($koneksi);
        }
        if (mysqli_query($koneksi, $notif)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $notif . "<br>" . mysqli_error($koneksi);
        }
    }
}

header("location:" . BASE_URL2 . "dashboard.php?id='$id'");
